import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import {BrowserRouter, Route, Link } from 'react-router-dom';
import Loadable from 'react-loadable';
// Component
import Button from './Button';
import Contenitore from './HOC/contenitore';

class App extends Component {
    render() {
        return(
            <div>
                <h1>REACT-REDUX!!</h1>
                <Button/>
                <Contenitore/>
            </div>
        );
    }
}

export default hot(module)(App);