import React, { Component } from 'react';
import { render } from 'react-dom';
// Redux
import { Provider } from 'react-redux';
import {store} from './Redux/store';
// App
import App from './App';

render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
