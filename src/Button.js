import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { connect } from 'redux';

import {toggleModal} from './Redux/actions';

const Button = ({open, toggleModal}) => {
    return (
      <button onClick={toggleModal}>REDUX</button>
    )
  }
  
  const mapStateToProps = state => {
    return {
      open : state.open
    }
  }
  
  /*const mapDispatchToProps = dispatch => {
    return {
      toggleModal: () => dispatch(toggleModal())
    }
  }*/

  const mapDispatchToProps = { toggleModal }

  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Button)
  