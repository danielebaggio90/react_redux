import React, { Component } from 'react';
import Button from '../Button';

const HOC = BaseComponent => (
    class HOC extends React.Component {
        state ={
            name: 'higher-order-component'
        };
        render() {
            return <div><BaseComponent {...this.state} {...this.props}/></div>;
        }
    }
);
export default HOC;