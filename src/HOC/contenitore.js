import React, { Component } from 'react';
import HOC from './HOC';

class Contenitore extends Component {
  render() {
    return (
      <div>{this.props.name}</div>
    )
  }
}
export default HOC(Contenitore);