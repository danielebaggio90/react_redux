
import {TOGGLE_MODAL} from './action-types';

const initialState = {
  open: false
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MODAL:
      console.log("REDUX");
      return {
        ...state,
        open: !state.open
      };
    default:
      return state;
  }
};
export default rootReducer;

