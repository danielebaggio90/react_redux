import {TOGGLE_MODAL} from './action-types';

export const toggleModal = () => ({ type: TOGGLE_MODAL });
