var fs = require('fs');
var express = require('express');
var mysql = require('mysql');
var form = require('./api/APIForm');
var table = require('./api/APITable');
var filter = require('./api/APIFilter');
var dashboard = require('./api/APIDashboard');
var app = express();

// API Table
app.get('/api/Web/getItemsTable/:mansioneFilter/:tipologiaFilter/:esperienzaFilter/:dataFilter/:orderBy/:ofset/:items', table.getItemsTable);
app.get('/api/Web/getAllItemsTable/:orderBy/:ofset/:items', table.getAllItemsTable);
app.get('/api/Web/getColumsTable', table.getColumsTable);
// API Dashboard
app.get('/api/Web/getItemsDashboard', dashboard.getItemsDashboard);
app.get('/api/Web/getPieValues/:CATEGORIA', dashboard.getPieValues);
// API Form
app.post('/api/Web/insertItemsForm/:obj', form.insertItemsForm);
app.get('/api/Web/getFormMansione', form.getFormMansione);
// API Filter
app.get('/api/Web/getFilterMansione', filter.getFilterMansione);
app.get('/api/Web/getFilterTipologia', filter.getFilterTipologia);
app.get('/api/Web/getOrderByFilter', filter.getOrderByFilter);

// Index.html
app.get('*', function (req, res) {
  fs.readFile('./index.html', 'utf-8', function(err, contenuto) {
    if (err) {
      console.log(err);
      return;
    }
    res.send(contenuto);
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
