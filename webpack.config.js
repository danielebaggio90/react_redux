var Visualizer = require('webpack-visualizer-plugin');
var path = require('path');
const webpack = require('webpack');
require('babel-polyfill');

module.exports = {
	devtool: 'eval-source-map',
    entry: ['babel-polyfill','./src/index.js'],
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', {
                                modules: false
                            }],
                            '@babel/preset-react',
                            ["@babel/preset-stage-2", { decoratorsLegacy: true }]
                        ],
                        plugins: ['@babel/plugin-proposal-class-properties','react-hot-loader/babel','syntax-dynamic-import','transform-react-loadable']
                    }
                }
            }
        ]
    },
	plugins: [new Visualizer({filename: './statistics.html'}), new webpack.HotModuleReplacementPlugin()],
	devServer: {
      hot: true,
    }
}
